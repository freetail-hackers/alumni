import React from 'react';

function Person(props) {
  const { name, gradYear, major, job, team, start, end, proPic, link } = props;
  let pp;
  if (proPic != null) {
    const source = require('../src/Images/' + proPic)
    pp = <img src={source} alt={name} className='proPic'/>
  }
  let website = name;
  if (link != null) {
    website = <a href={link} target="_blank" rel="noreferrer">{name}</a>
  }

  return (
    <div className="person">
      <div className="ppContainer">
        {pp}
      </div>
      <h2>{website}</h2>
      <p>{major} Major, Class of {gradYear}</p>
      <p>{team} Team | {start} - {end != null ? end : "now"}</p>
      <p>Currently: {job}</p>
    </div>
  );
}


export default Person;