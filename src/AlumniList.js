import React from 'react';
import Person from './Person';

function AlumniList({teamsToShow, teamOrder}) {


  let alumni = [
    {
      name: 'John Smith',
      gradYear: '2020',
      major: 'Computer Science',
      job: 'Software Engineer',
      team: 'Tech',
      start: '2018',
      end: '2020',
    },
    {
      name: 'Jane Doe',
      gradYear: '2018',
      major: 'Marketing',
      job: 'Marketing Manager',
      team: 'Creative',
      start: '2015',
      end: '2016',
    },
    {
      name: 'Trish Truong',
      gradYear: '2025',
      major: 'Computer Science',
      job: 'Student',
      team: 'Tech',
      start: '2023',
    },
    {
      name: 'Ryan Gosling',
      gradYear: '2016',
      major: 'Design',
      job: 'Actor',
      team: 'Creative',
      start: '2013',
      end: '2015',
      link: 'https://twitter.com/RyanGosling'
    },
    {
      name: 'Charles Miele',
      gradYear: '2025',
      major: 'Economics',
      job: 'Research Assistant',
      team: 'Tech',
      start: '2021',
      proPic: '4.png',
      link: 'https://www.charlesmiele.com'
    },
    {
      name: 'Jane Doe',
      gradYear: '2018',
      major: 'Marketing',
      job: 'Marketing Manager',
      team: 'Creative',
      start: '2015',
      end: '2016',
    },
    {
      name: 'Trish Truong',
      gradYear: '2025',
      major: 'Computer Science',
      job: 'Student',
      team: 'Tech',
      start: '2023',
    },
    {
      name: 'Ryan Gosling',
      gradYear: '2016',
      major: 'Design',
      job: 'Actor',
      team: 'Creative',
      start: '2013',
      end: '2015',

    },
    {
      name: 'John Smith',
      gradYear: '2020',
      major: 'Computer Science',
      job: 'Software Engineer',
      team: 'Tech',
      start: '2018',
      end: '2020',
    },
    {
      name: 'Jane Doe',
      gradYear: '2018',
      major: 'Marketing',
      job: 'Marketing Manager',
      team: 'Creative',
      start: '2015',
      end: '2016',
    },
    {
      name: 'Trish Truong',
      gradYear: '2025',
      major: 'Computer Science',
      job: 'Student',
      team: 'Tech',
      start: '2023',
    },
    {
      name: 'Ryan Gosling',
      gradYear: '2016',
      major: 'Design',
      job: 'Actor',
      team: 'Creative',
      start: '2013',
      end: '2015',
    }
    // Add more alumni objects as needed
  ];

  if (teamsToShow !== true){
    let possibleTeams = [];
    for (let i = 0; i < teamsToShow.length; i++) {
      if (teamsToShow[i]) {
        possibleTeams.push(teamOrder[i].toLowerCase())
      }
    }
    console.log(possibleTeams)
    // Assumes identical casing
    alumni = alumni.filter(obj => possibleTeams.includes(obj.team.toLowerCase()));
    console.log(alumni)
  }

  return (
    <div className="alumniList">
      {alumni.map((person, index) => (
        <Person
          key={index}
          name={person.name}
          gradYear={person.gradYear}
          major={person.major}
          job={person.job}
          team={person.team}
          start={person.start}
          end={person.end}
          proPic={person.proPic}
          link={person.link}
        />
      ))}
    </div>
  );
}

export default AlumniList;