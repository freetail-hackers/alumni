import React, { useState } from 'react';
import AlumniList from './AlumniList'

function App() {
  const [clickStatus, setClickStatus] = useState([false, false, false, false, false, false])

  const teams = ["Corporate", "Creative", "Logistics", "Outreach", "Tech", "Eventum"]


  return (
    <div className="App">
      <div className="tabContainer">
        <h1>Select Team(s)</h1>
        {teams.map((teamName, index) =>
          (
            <button
              key={teamName}
              style={{backgroundColor: clickStatus[index] ? 'blue' : 'white', color: clickStatus[index] ? 'white' : 'blue'}}
              onClick={() => {setClickStatus([...clickStatus.slice(0, index), !clickStatus[index], ...clickStatus.slice(index + 1)])}}>
                {teamName.toUpperCase()}
            </button>
        ))}
      </div>
      <AlumniList teamsToShow={clickStatus.every(val => !val) ? true : clickStatus} teamOrder={teams}/>
    </div>
  );
}

export default App;